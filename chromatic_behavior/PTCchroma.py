"""
This script reads the data from a PTC file, and save them to a data frame, so they are accessible through other scripts.
The file we use here were produced in :
/afs/cern.ch/work/n/natriant/private/workspaces/dpp_iteration_chroma_match_randomb3b5b70.25/scratch0/sixtrack_input/w1/
SPS_VCC0.0_b3b5b7_random0.25_5D_Z200.0_DPP0.000_QXP2.0_QYP1.0_Q26_EN26000_fma/mad.mad6t.sh.lxplus117.cern.ch.YFEjIk/test
"""


def extract_the_data_from_PTC(PTC_file):
    DPP = []
    QX0 = []
    QY0 = []

    with open(PTC_file) as f:
        for line in f:
            line = line.strip()  # remove the empty characters from the string
            print line
            line = line.split(" ")  # split the string in a list
            while '' in line:
                line.remove('')  # remove the empty characters from the list

            DPP.append(float(line[0]))
            QX0.append(float(line[1]))
            QY0.append(float(line[2]))
    return DPP, QX0, QY0
