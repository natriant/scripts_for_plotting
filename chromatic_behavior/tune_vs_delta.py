import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from PTCchroma import *

# parameters for the study
SCAN_DELTA = np.arange(-5e-3, 6e-3, 1e-3)
z = '200.0'
status_type = "5D"
color_pointer = 0
random = 0.25
QXP = 2.0
QYP = 1.0

# path to the study
path_to_result_folder = '/afs/cern.ch/work/n/natriant/private/workspaces/dpp_iteration_chroma_match_randomb3b5b70.25/scratch0/w1/sixjobs/plotting/different_seeds'  # example
study_name = '/dpp_iteration_chroma_match_randomb3b5b70.25/'  # example

# parameters for plotting
amplitudes = ['1_2', '2_3', '3_4', '4_5', '5_6']
amplitudes = ['2_3']
sigma = '2'

# parameters for the color
jet = plt.get_cmap('jet')
colors_x = iter(jet(np.linspace(0, 1, len(SCAN_DELTA))))
colors_y = iter(jet(np.linspace(0, 1, len(SCAN_DELTA) * 2)))

# flags for functions
compare_with_PTC = True
PTC_file = "PTC_chroma"

fig, ax = plt.subplots(figsize=(8.5, 6.5))
handles_qx = []
for amplitude, color in zip(amplitudes, colors_x):

    tune_x_values = []
    tune_y_values = []

    for j in SCAN_DELTA[:]:
        j = "{:.3f}".format(j)

        result_folder = path_to_result_folder + study_name + "SPS_VCC0.0_b3b5b7_random%s_%s_Z%s_DPP%s_QXP%s_QYP%s_Q26_EN26000_fma" % (
        random, status_type, z, j, QXP, QYP)

        df = pd.read_csv(result_folder, delim_whitespace=True, header=None)
        df.columns = ['id', 'file', 'qx', 'qy', 'dq', 'x0', 'y0', 'angle', 'amplitude', 'amplitude_1']

        df = df[(df['id'] == 1)]
        df = df[(df['angle'] == 45)]
        df = df[(df['amplitude'] == amplitude)]
        df = df.reset_index(drop=True)
        tune_x_values.append(df['qx'])
        tune_y_values.append(df['qy'])

    if len(amplitudes) > 1:

        c1 = next(colors_x)
        c2 = next(colors_y)
        plt.scatter(SCAN_DELTA, tune_x_values, s=10, edgecolors=None, c=c1, label=amplitude[0] + r"$ \sigma$")
        # plt.scatter(SCAN_DELTA, tune_y_values, s=10, edgecolors=None, c=c2', label=amplitude[0]+r"$ \sigma$")

        name_of_figure = "Qx_vs_delta_{}sigma_z{}.png".format(sigma, z)
        #name_of_figure = "Qy_vs_delta_{}sigma_z{}.png".format(sigma, z)

    else:
        if compare_with_PTC:
            plt.plot(SCAN_DELTA, tune_x_values, marker='o', c='r')
            plt.plot(SCAN_DELTA, tune_y_values, marker='o', c='b')
        else:
            plt.plot(SCAN_DELTA, tune_x_values, marker='o', c='r', label=r"$\rm Q_x$")
            plt.plot(SCAN_DELTA, tune_y_values, marker='o', c='b', label=r"$\rm Q_y$")
        plt.ylim(0.10, 0.22)
        name_of_figure = "Qx_Qy_vs_delta_{}sigma_z{}.png".format(sigma, z)

    if compare_with_PTC:
        DPP, QX0, QY0 = extract_the_data_from_PTC(PTC_file)
        plt.plot(DPP, QX0, marker = '^', c='r')
        plt.plot(DPP, QY0, marker = '^', c='b')
        # plots for the label
        plt.axhline(y=-1,c='r',label= r"$ \rm Q_x$")
        plt.axhline(y=-1, c='b', label=r"$ \rm Q_y$")
        plt.axhline(y=-1, c='k', marker = 'o', label="SixTrack")
        plt.axhline(y=-1, c='k', marker='^', label="MAD-X/PTC")


plt.xlim(-0.010, 0.010)
plt.legend(loc=7)  # center right

plt.title('SPS, 26GeV, CC 0MV, b3b5b7,random{}_{},\n QXP={}, QYP={}, Z={}, {}sigma'.format(random, status_type, QXP, QYP, z, sigma))
plt.xlabel(r"$ \rm delta$", fontsize=14)
plt.ylabel(r"$\rm Q_x, Q_y$", fontsize=14)
plt.tick_params(labelsize=13)
plt.tight_layout()
if compare_with_PTC:
    PTC = 'PTC'
else:
    PTC = ''
plt.savefig("./figures_for_tune_vs_delta/" + PTC + name_of_figure)
plt.show()
plt.close()

quit()
