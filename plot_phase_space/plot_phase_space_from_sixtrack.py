"""
Python 2.7

With this script one can plot the phase space for a particle obtaining its coordinates from Sixtrack.
The coordinates are taken from the files: NORM_IP3_DUMP_1 that contains the normalised coordinates.

To create such a file, go to a track directory and copy fort.2 and fort.3 in a test directory. Run the sixtrack
executable.
eg:
/afs/cern.ch/work/n/natriant/private/workspaces/ripples50Hz_1QF/scratch0/w1/track/SPS_VCC0.0_nomult_5D_Z100.0_DPP0.0_
QXP0.0_QYP0.0_Q26_EN26000_fma_RIPP1D-11_869Hz/1/simul/26.13_26.18/1_2/e4/45/tune_shift_test
"""
import matplotlib.pyplot as plt
import pandas as pd

data_file = 'NORM_IP3_DUMP_1'
study_name = "phsase_space_ripples_many_QFs_second_try"

df = pd.read_csv(open('./my_data/' + data_file), delim_whitespace=True, comment='#', header=None)
df.columns = ['id', 'turn', 'pos[m]', 'nx[1.e-3 sqrt(m)]', 'npx[1.e-3 sqrt(m)]', 'ny[1.e-3 sqrt(m)]',
              'npy[1.e-3 sqrt(m)]', 'nsig[1.e-3 sqrt(m)]', 'ndp/p[1.e-3 sqrt(m)]', 'kt']


x = df['nx[1.e-3 sqrt(m)]']
px = df['npx[1.e-3 sqrt(m)]']
y = df['ny[1.e-3 sqrt(m)]']
py = df['npy[1.e-3 sqrt(m)]']

# Horizontal phase space
plt.subplot(1, 2, 1)  # 1 rows, 2 columns , Plot 1
plt.plot(x,px)
plt.xlabel("XN[1e-3 sqrt(m)]")
plt.ylabel("XPN[1e-3 sqrt(m)]")
plt.xlim(-0.2, 0.2)
plt.ylim(-0.2, 0.2)
plt.axis('scaled')

# Vertical phase space
plt.subplot(1, 2, 2)  # 1 rows, 2 columns , Plot 2
plt.plot(y,py)
plt.xlabel("YN[1e-3 sqrt(m)]")
plt.ylabel("YPN[1e-3 sqrt(m)]")
plt.xlim(-0.2, 0.2)
plt.ylim(-0.2, 0.2)
plt.axis('scaled')

plt.tight_layout()
plt.suptitle("Normalized phase space", fontsize = 14)
plt.savefig("./figures/" + study_name + ".png")


plt.show()
