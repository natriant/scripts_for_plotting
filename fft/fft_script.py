import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import NAFFlib as naff  #NAFF lib from kostas. you can install pip install nafflib in your current directory.

frev =  43450
turns = 2000


name_columns = ['ID', 'turn', 's[m]', 'x[mm]', 'xp[mrad]', 'y[mm]', 'yp[mrad]', 'z[mm]', 'dE/E[1]', 'ktrack']
#name_columns = [' id', 'turn', 'pos[m]' ,'nx[1.e-3 sqrt(m)]', 'npx[1.e-3 sqrt(m)]', 'ny[1.e-3 sqrt(m)]' ,'npy[1.e-3 sqrt(m)]',' nsig[1.e-3 sqrt(m)]' ,'ndp/p[1.e-3' ,'sqrt(m)]' ,'k']
df = pd.read_csv(open('IP3_DUMP_1'),delim_whitespace = True, comment = '#', names = name_columns)
#df = pd.read_csv(open('NORM_IP3_DUMP_1'),delim_whitespace = True, comment = '#', names = name_columns)
#print df
df = df[df['ID']==1] #keep only particle 1
df.reset_index()


Q_x = []
A_x = []
B_x = []

Q_y = []

j=0
turns_list = []
for i in range(50,turns+1,50):
	
	df2 = df[j:i]
	 
	        
	turns_list.append(j+1)

 	Qx, Ax, Bx =  naff.get_tunes(np.array(df2['x[mm]']),1)
	Qy, Ay, By = naff.get_tunes(np.array(df2['y[mm]']),1)
         
        Q_y.append(Qy)  

        Q_x.append(Qx)
	A_x.append(Ax)
	B_x.append(Bx)
        j = i
	 
	
fig, axes = plt.subplots( figsize=(8.5,6.5))
pointer = 0
#for i in range(0,len(Q_x)):
#	k = Q_x[i][0]
#	plt.plot(turns,k, markersize = 12, linestyle = '-')
	#print Q_x[1][0]
Q_x_abs = [abs(x) for x in Q_x]
Q_y_abs = [abs(x) for x in Q_y]

plt.plot(turns_list, Q_x_abs)

for i in range(0,len(Q_x)):
	plt.vlines(x = turns_list[i],ymin=0.1190,ymax = Q_x[i])
plt.axvline(x=869, c ='r', label = "turn 869")
plt.title("Tune shift during 2000 turns")
plt.xlim(0,2000)
plt.ylim(0.1190,0.14)
plt.xlabel("turns")
plt.ylabel("Qx")
plt.legend()
plt.savefig("tune shift_Qx.png")
#plt.show()

fig, axes = plt.subplots( figsize=(8.5,6.5))
pointer = 0
#for i in range(0,len(Q_x)):
#       k = Q_x[i][0]
#       plt.plot(turns,k, markersize = 12, linestyle = '-')
        #print Q_x[1][0]
plt.plot(turns_list, Q_y_abs)

for i in range(0,len(Q_y)):
        plt.vlines(x=turns_list[i] ,ymin=0.1190,ymax = abs(Q_y[i]))
plt.axvline(x=869, c ='r', label = "turn 869")
plt.title("Tune shift during 2000 turns")
plt.xlim(0,2000)
plt.ylim(0.175, 0.185)
plt.xlabel("turns")
plt.ylabel("Qy")
plt.legend()
plt.savefig("tune shift_Qy.png")

quit()





