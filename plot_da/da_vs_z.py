import glob
import os
import re
import sys
import numpy as np
import scipy
sys.path.append('/afs/cern.ch/user/s/skostogl/public/SPS_sixtrack/sixdb/SixDeskDB/')
import sixdeskdb
import matplotlib
import matplotlib.pyplot as plt

def float_range(start, stop, increment):
    while start < stop: 
        yield start
        start += increment

f, ax = plt.subplots(1,figsize=(9,9))

# general parameters of the current study
mask_prefix = 'SPS'
Qxp = '0.0'
Qyp = '0.0'
optics = '26'
deg ='0.0'
dpp = '0.0'
multipoles = 'b3b5'
status = '6D'
energy = 26000.
da_fma = 'da'

# path to the current studies
path_to_studies = '/afs/cern.ch/work/n/natriant/private/workspaces/SPS_da_DYNK/scratch0/w1/sixjobs'

# specific parameters of the study
SCAN_V = np.arange(0.5,3.0,0.5)
SCAN_Z = np.arange(0.,700.,100.)

# parameters for the colorbar
jet= plt.get_cmap('jet')
colors = iter(jet(np.linspace(0,1,len(SCAN_V))))


for j in SCAN_V:

  studies =[]
  z_values = []
  min_da = []

  for i in SCAN_Z:
    z_current_value = '%s'%(i)
    new_name = 'SPS_VCC%s_%s_%s_Z%s_DPP%s_QXP%s_QYP%s_%s_DEG%s_EN%s_%s' % (j, multipoles, status, z_current_value, dpp, Qxp, Qyp, optics, deg, energy, da_fma)
    studies.append(new_name)
    z_values.append(z_current_value)

  for study in studies: 
    
    # check if the ".db" file exists already
    # if it exists, load the file
    # if not create it  
    if os.path.exists(study+'.db'):
      # file exists
      db=sixdeskdb.SixDeskDB(study+'.db') 
    else:
      db = sixdeskdb.SixDeskDB.from_dir(path_to_studies+'/studies/'+study+'/')
     
    db.mk_da()
    seed,angle,da=db.get_da_angle(alost = 'alost2').T
    #min_current_da = min([abs(i) for i in da])
    dasig = da
    dasig=min(dasig.T[0])
    min_da.append(dasig)

  c1 = next(colors)  
  ax.scatter(z_values,min_da,c=c1, marker='o', label =str(j)+"_MV")
  print "vcc", j
  print "z", z_values
  print "minda", min_da

title1 = 'SPS 26 GeV, b3b5 errors, 1E6 turns, RF: 2MV, 6D, DYNK_CC 300, '
ax.set_ylabel("min DA $ [\sigma]$", fontsize = 16)
ax.set_xlabel("z_max[mm]", fontsize = 16)
plt.title(title1+r'$\phi cc1 = \phi cc2 $'+'=0', fontsize=14)
ax.xaxis.set_tick_params(labelsize=14)
ax.yaxis.set_tick_params(labelsize=14)
plt.ylim([0,35])
plt.legend()
f.tight_layout()

directory = './figures'
name = directory + "/da_vs_z_scanVcc.png"
plt.savefig(name)
plt.show()
