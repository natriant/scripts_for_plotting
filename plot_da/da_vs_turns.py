import glob
import os
import re
import sys
import numpy as np
import scipy
sys.path.append('/afs/cern.ch/user/s/skostogl/public/SPS_sixtrack/sixdb/SixDeskDB/')
import sixdeskdb
import matplotlib
import matplotlib.pyplot as plt

def float_range(start, stop, increment):
    while start < stop: 
        yield start
        start += increment


def RunDaVsTurns(db,force,outfile,outfileold,turnstep,davstfit,fitdat,fitdaterr,fitndrop,fitskap,fitekap,fitdkap,outfilefit):
  '''Da vs turns -- calculate da vs turns for study dbname, if davstfit=True also fit the data'''
  #---- calculate the da vs turns
  try:
    turnstep=int(float(turnstep))
  except [ValueError,NameError,TypeError]:
    print('Error in RunDaVsTurns: turnstep must be an integer values!')
    sys.exit(0)
  if(not db.check_seeds()):
    print('!!! Seeds are missing in database !!!')
  turnsl=db.env_var['turnsl']#get turnsl for outputfile names
  turnse=db.env_var['turnse']
  for seed in db.get_db_seeds():
    seed=int(seed)
    print('analyzing seed {0} ...').format(str(seed))
    for tune in db.get_db_tunes():
      print('analyzing tune {0} ...').format(str(tune))
      dirname=db.mk_analysis_dir(seed,tune)#directory struct already created in clean_dir_da_vst, only get dir name (string) here
      print('... get survival data')
      dasurv= db.get_surv(seed,tune)
      if dasurv is None:
        print("ERROR: survival data could not be retrieved due to "+
              "and error in the database or tracking data. Skip "
              "this seed %s"%(seed))
        continue
      print('... get da vs turns data')
      daout = db.get_da_vst(seed,tune)
      if(len(daout)>0):#reload data, if input data has changed redo the analysis
        an_mtime=daout['mtime'].min()
        res_mtime=db.execute('SELECT max(mtime) FROM six_results')[0][0]
        if res_mtime>an_mtime or force is True:
          files=('DA.%s.out DAsurv.%s.out DA.%s.png DAsurv.%s.png DAsurv_log.%s.png DAsurv_comp.%s.png DAsurv_comp_log.%s.png'%(turnse,turnse,turnse,turnse,turnse,turnse,turnse)).split()+['DA.out','DAsurv.out','DA.png','DAsurv.png','DAsurv_log.png','DAsurv_comp.png','DAsurv_comp_log.png']
          clean_dir_da_vst(db,files)# create directory structure and delete old files
          print('... input data has changed or force=True - recalculate da vs turns')
          daout=mk_da_vst(dasurv,seed,tune,turnsl,turnstep)
          print('.... save data in database')
          #check if old table name da_vsturn exists, if yes delete it
          if(db.check_table('da_vsturn')):
            print('... delete old table da_vsturn - table will be substituted by new table da_vst')
            db.execute("DROP TABLE da_vsturn")
          db.st_da_vst(daout,recreate=True)
      else:#create data
        print('... calculate da vs turns')
        daout=mk_da_vst(dasurv,seed,tune,turnsl,turnstep)
        print('.... save data in database')
        db.st_da_vst(daout,recreate=False)
      if(outfile):# create dasurv.out and da.out files
        fnsurv='%s/DAsurv.%s.out'%(dirname,turnse)
        save_dasurv(dasurv,fnsurv)
        print('... save survival data in {0}').format(fnsurv)
        fndaout='%s/DA.%s.out'%(dirname,turnse)
        save_daout(daout,fndaout)
        print('... save da vs turns data in {0}').format(fndaout)
      if(outfileold):
        fndaoutold='%s/DAold.%s.out'%(dirname,turnse)
        save_daout_old(daout,fndaoutold)
        print('... save da vs turns (old data format) data in {0}').format(fndaoutold)

f, ax = plt.subplots(1,figsize=(9,9))
mask_prefix = 'SPS'
Qxp = '0.0'
Qyp = '0.0'
optics = '26'
deg ='0.0'
dpp = '0.0'
multipoles = 'b3b5'
status = '6D'
energy = 26000.
da_fma = 'da'

path_to_studies = '/afs/cern.ch/work/n/natriant/private/workspaces/SPS_da/scratch0/w1/sixjobs'

#SCAN_V = np.arange(0.5,3.0,0.5)
SCAN_V = [1.5]
SCAN_Z = [0.0]
#SCAN_Z = np.arange(0.,700.,100.)
colors = ['b','g','r','c','m']
k_for_color =0 

for j in SCAN_V:
  print j 
  

  #main_name = 'SPS'+new_name+'_b3b5_6D_'
  studies =[]
  z_values = []
  min_da = []

  for i in SCAN_Z:
    z_current_value = '%s'%(i)
    new_name = 'SPS_VCC%s_%s_%s_Z%s_DPP%s_QXP%s_QYP%s_%s_DEG%s_EN%s_%s' % (j, multipoles, status, z_current_value, dpp, Qxp, Qyp, optics, deg, energy, da_fma)
    studies.append(new_name)
    z_values.append(z_current_value)

  for study in studies: 
    print study 

    #A. load_dir
    db=sixdeskdb.SixDeskDB.from_dir(path_to_studies+'/studies/'+study+'/')  #like load_dir in sixdb

    #B. find the min da --> da is calculated for 1e6 turns, we dont want this one --> comment out
    #db.mk_da()
    #seed,angle,da=db.get_da_angle(alost = 'alost2').T
    #dasig = da
    #dasig=min(dasig.T[0])
    #min_da.append(dasig)

    #C. da_v_turns
    
    #define default values (from da_vs_turns function sto sixdb)
    force      = False
    turnstep   = 100
    outfile    = False
    outfileold = False
    outfilefit = False
    davstfit   = False
    fitdat     = 'dawsimp'
    fitdaterr  = 'dawsimperr'
    fitndrop   = 25
    fitskap    = -5.0
    fitekap    = 5.0
    fitdkap    = 0.01
    if '-force' in args:      force=True
    if '-turnstep' in args:   turnstep=args[args.index('-turnstep')+1]
    if '-outfile' in args:    outfile=True
    if '-outfileold' in args: outfileold=True
    if '-outfilefit' in args: outfilefit=True
    if '-fit' in args:        davstfit=True
    if '-fitopt' in args:
      davstfit=True
      fitdat    = args[args.index('-fitopt')+1]
      fitdaterr = args[args.index('-fitopt')+2]
      fitndrop  = args[args.index('-fitopt')+3]
      fitskap   = args[args.index('-fitopt')+4]
      fitekap   = args[args.index('-fitopt')+5]
      fitdkap   = args[args.index('-fitopt')+6]
  
    RunDaVsTurns(db,force,outfile,outfileold,turnstep,davstfit,fitdat,fitdaterr,fitndrop,fitskap,fitekap,fitdkap,outfilefit)  

  quit()  

  print "studies", studies
  print "z", z_values
  print "min_da", min_da




  
  ax.scatter(z_values,min_da,color=colors[k_for_color], marker='o', label =str(j)+"_MV")
  k_for_color = k_for_color+1

title1 = '=90, 26 GeV, 1E6 turns, RF: 2MV, 6D'
ax.set_ylabel("min DA $ [\sigma]$", fontsize = 16)
ax.set_xlabel("z_max[mm]", fontsize = 16)
plt.title(r'$\phi cc1 = \phi cc2 $'+ title1 , fontsize=14)
ax.xaxis.set_tick_params(labelsize=14)
ax.yaxis.set_tick_params(labelsize=14)
plt.ylim([0,35])
plt.legend()
f.tight_layout()
directory = "/afs/cern.ch/work/n/natriant/private/workspaces/SPS_da/scratch0/w1/sixjobs/day_plots/figures"
name = directory + "/90deg.png"
  #  name = directory + "/x_px.png"
plt.savefig(name)
  #  plt.close()
plt.show()
