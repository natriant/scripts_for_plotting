import numpy as np
import glob, os, fnmatch
import zipfile
import csv
import re
import pandas as pd
import zipfile
import matplotlib.pyplot as plt
import sys
import os.path

from footprint import *

# specific parameters of the study
#SCAN_Z     = np.arange(4.,500.,100.)
SCAN_DELTA = [0.0]
#SCAN_DELTA = np.arange(-5e-3,6e-3,1e-3)
SCAN_Z = [100.0]

colors = ["k","r","b","g","m","y","c", "gray", "tomato", "orangered", "seagreen", "brown","maroon","coral" ]

# general parameters of the study
RIPP_depth = 0.0
status_type = "5D"
color_pointer = 0
QXP = 0.0
mult = 'b3b5'
RIPP = '1D-3'


for j in SCAN_Z[:]:

  j = "{:.3f}".format(j)

  track_dir = "/afs/cern.ch/work/n/natriant/private/workspaces/ripples50Hz_1QF/scratch0/w1/track/SPS_VCC0.0_%s_%s_Z%s_DPP0.0_QXP0.0_QYP0.0_Q26_EN26000_fma_RIPP%s_869Hz/1/simul/26.13_26.18"%(mult,status_type,j,RIPP)

  
  flag_read_fmas = True

  if flag_read_fmas:
    zipf_name = "Sixout.zip"
    nb_zip    = 0
    particles = 0
    result_folder = "SPS_VCC0.0_{}_{}_Z{}_DPP0.0_QXP0.0_QYP0.0_Q26_EN26000_fma_ripp1e%s".format(mult,status_type,str(j),RIPP)
    df_all = pd.DataFrame()
    list_ = []
    with open(result_folder,'wb') as f_res:
      for root, dirnames, filenames in os.walk(track_dir):
        for filename in fnmatch.filter(filenames, 'Sixout.zip'):
          zip_dir = os.path.join(root, filename)
          nb_zip +=1
          path_list = (root+"/"+zipf_name).split(os.sep)
          print "Unzipped file: ", zipf_name
          bpms_position = []
          print "Full path: ", root+"/"+zipf_name
          z = zipfile.ZipFile(root+"/"+zipf_name)
          df = pd.read_csv(z.open('fma_sixtrack'), delim_whitespace=True, comment='#', header=None)
          angle = path_list[-2]
          amplitude = path_list[-4]
          amplitude_1 = float(amplitude[0])
          
          
          df.columns = ["inputfile","method","id", "q1", "q2", "q3", "eps1_min", "eps2_min", "eps3_min", "eps1_max", "eps2_max", "eps3_max", "eps1_avg", "eps2_avg", "eps3_avg", "eps1_0", "eps2_0", "eps3_0", "phi1_0", "phi2_0", "phi3_0", "norm_flag", "first_turn", "last_turn"]
          qx1 = np.array(df[df['inputfile'] == 'IP3_DUMP_1']['q1'])
          qy1 = np.array(df[df['inputfile'] == 'IP3_DUMP_1']['q2'])
          qx2 = np.array(df[df['inputfile'] == 'IP3_DUMP_2']['q1'])
          qy2 = np.array(df[df['inputfile'] == 'IP3_DUMP_2']['q2'])
          id = np.array(df[df['inputfile'] == 'IP3_DUMP_2']['id'])
          file = np.array(df[df['inputfile'] == 'IP3_DUMP_2']['inputfile'])
          diffx = [(x1-x2)**2 for x1,x2 in zip(qx1,qx2)]
          diffy = [(y1-y2)**2 for y1,y2 in zip(qy1,qy2)]
          dq = [np.log10( np.sqrt(a1 + a2) ) for a1,a2 in zip(diffx, diffy)]
  
          x0 = np.array(df[df['inputfile'] == 'IP3_DUMP_1']['eps1_0'])
          y0 = np.array(df[df['inputfile'] == 'IP3_DUMP_1']['eps2_0'])
          for i in range(len(qx2)):
            f_res.write("%s %s %s %s %s %s %s %s %s %s\n" %(id[i], file[i],qx2[i], qy2[i], dq[i], x0[i],y0[i], angle,amplitude, amplitude_1))
            f_res.flush()
 
 
  flag_plot_fma = True
  if flag_plot_fma:
    result_folder = "SPS_VCC0.0_{}_{}_Z{}_DPP0.0_QXP0.0_QYP0.0_Q26_EN26000_fma_ripp1e%s".format(mult,status_type,str(j),RIPP)
    
    df = pd.read_csv(result_folder, delim_whitespace=True,header=None)
    df.columns = ['id', 'file','qx', 'qy', 'dq','x0','y0', 'angle', 'amplitude','amplitude_1']
    
    fig, axes = plt.subplots( figsize=(8.5,6.5)) #auto itan coment out anyway
    pointer = 0
    
    #fig, axes = plt.subplots( figsize=(14.5,12.5))
    #for key, group in df.groupby('angle'):
    #  if pointer == 0:
    #    plt.plot(group['qx'],group['qy'], c = colors[color_pointer],label='delta ='+'{:.3f}'.format(j), linewidth=0.3)
    #    pointer = 1
    #  plt.plot(group['qx'],group['qy'], c = colors[color_pointer],label='', linewidth=0.3)
    #for key, group in df.groupby(['amplitude', 'id']):
    #  plt.plot(group['qx'],group['qy'], c = colors[color_pointer],label='', linewidth=0.3)
 
    #plt.plot(df['qx'],df['qy'],c = colors[color_pointer],label='delta ='+'{:.3f}'.format(j))#, linewidth=0.3)
    #plt.scatter(df['qx'], df['qy'], s=2, edgecolors=None,c = colors[color_pointer], label='delta ='+'{:.3f}'.format(j))
     
    #plt.scatter(df['qx'], df['qy'], s=2, edgecolors=None,c = df['amplitude_1'], label='delta ='+'{:.3f}'.format(j),cmap='jet')   #for amplitdue color map
     
    vmin = -3.0
    vmax = -7.0
    plt.scatter(df['qx'], df['qy'], s=2, edgecolors=None,c = df['dq'], label='z ='+'{:.3f}'.format(j), cmap='jet')#,vmin=vmin,vmax=vmax,cmap='jet')  #for diffuson map
    cbar=plt.colorbar()
    cbar.set_label(r'$\max_{j=1,2}(\log10{(\max_{i=1}^{\rm nfma}|Q_{j,i}-\bar Q_{j}|)}$',labelpad=40,rotation=270,fontsize=12)
    #cbar.set_label('amplitude [sigma]', labelpad=40, rotation=270,fontsize=12) # if you want the colorbar based on amplitude and not on the diffusion

    color_pointer = color_pointer+1
    plt.legend()

    # comment out the lines below if you want to plot the resoanance lines
    '''
    n=3
    
    plot_res_upto_order(6, l=0, qz=0.0056,c1='darkgrey', c2='darkgrey', c3='r',annotate=False)
    for ll in range (1,6):
      plot_res_order_specific([n], l=ll, qz=0.0056,c1='darkgrey', c2='darkgrey', c3='r',annotate=False)
    n=7
    for ll in range (1,7):
      plot_res_order_specific([n],list=[(4,3)], l=ll, qz=0.0056,c1='darkgrey', c2='darkgrey', c3='g',annotate=False)
    plt.tight_layout()
    '''
    plt.xlim([0.10, 0.16])
    plt.ylim([0.14, 0.21])
        
    plt.title('SPS, 26GeV, noCC, RIPP{} , {}, {}, QXP=0.0, QYP =0.0'.format(RIPP,mult,status_type))
    plt.xlabel(r"$ \rm Q_x$", fontsize=14)
    plt.ylabel(r"$\rm Q_y$", fontsize=14)
    plt.tick_params(labelsize=13)
    plt.tight_layout()
    plt.savefig("Z%s_fmas_noCC_%s_%s_QXP0.0_delta0_RIPP%s.png"%(j,mult,status_type,RIPP))
    #plt.show()
    plt.close()
#plt.show()  
quit()  
    
    
    
    
    
