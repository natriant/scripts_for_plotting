import numpy as np
import glob, os, fnmatch
import zipfile
import csv
import re
import pandas as pd
import zipfile
import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt
import sys
import os.path

from footprint import *

# SCAN_Z     = np.arange(-600.,700.,100.)
# SCAN_DELTA = [0.0,1e-3,2e-3]
SCAN_DELTA = np.arange(-5e-3, 6e-3, 1e-3)

# SCAN_DELTA = ['-0.003','0.000','0.003']
# SCAN_Z = [0.0]
# colors = ["k","r","b","g","m","y","c", "gray", "tomato", "orangered", "seagreen", "brown","maroon","coral" ]

# parameters for the colorbar
jet = plt.get_cmap('jet')
colors = iter(jet(np.linspace(0, 1, len(SCAN_DELTA))))

status_type = "5D"
color_pointer = 0
random = 0.25
fig = plt.subplots(figsize=(8.5, 6.5))
# fig = plt.figure()


# SPS_VCC0.0_b3b5b7_random0.25_5D_Z0.0_DPP0.001_QXP0.0_QYP0.0_Q26_EN26000_fma
for j in SCAN_DELTA[:]:
    j = "{:.3f}".format(j)
    track_dir = "/afs/cern.ch/work/n/natriant/private/workspaces/dpp_iteration/scratch0/w1/track/SPS_VCC0.0_b3b5b7_random%s_%s_Z0.0_DPP%s_QXP0.0_QYP0.0_Q26_EN26000_fma/1/simul/26.13_26.18" % (
    random, status_type, j)

    flag_read_fmas = False

    if flag_read_fmas:
        zipf_name = "Sixout.zip"
        nb_zip = 0
        particles = 0
        result_folder = "SPS_VCC0.0_b3b5b7_random%s_%s_Z0.0_DPP%s_QXP0.0_QYP0.0_Q26_EN26000_fma" % (
        random, status_type, j)
        df_all = pd.DataFrame()
        list_ = []
        with open(result_folder, 'wb') as f_res:
            for root, dirnames, filenames in os.walk(track_dir):
                for filename in fnmatch.filter(filenames, 'Sixout.zip'):
                    zip_dir = os.path.join(root, filename)
                    nb_zip += 1
                    path_list = (root + "/" + zipf_name).split(os.sep)
                    print "Unzipped file: ", zipf_name
                    bpms_position = []
                    print "Full path: ", root + "/" + zipf_name
                    z = zipfile.ZipFile(root + "/" + zipf_name)
                    df = pd.read_csv(z.open('fma_sixtrack'), delim_whitespace=True, comment='#', header=None)
                    angle = path_list[-2]
                    amplitude = path_list[-4]
                    # print amplitude
                    amplitude_1 = float(amplitude[0])
                    # print "heree", amplitude_1
                    # quit()

                    df.columns = ["inputfile", "method", "id", "q1", "q2", "q3", "eps1_min", "eps2_min", "eps3_min",
                                  "eps1_max", "eps2_max", "eps3_max", "eps1_avg", "eps2_avg", "eps3_avg", "eps1_0",
                                  "eps2_0", "eps3_0", "phi1_0", "phi2_0", "phi3_0", "norm_flag", "first_turn",
                                  "last_turn"]
                    qx1 = np.array(df[df['inputfile'] == 'IP3_DUMP_1']['q1'])
                    qy1 = np.array(df[df['inputfile'] == 'IP3_DUMP_1']['q2'])
                    qx2 = np.array(df[df['inputfile'] == 'IP3_DUMP_2']['q1'])
                    qy2 = np.array(df[df['inputfile'] == 'IP3_DUMP_2']['q2'])
                    id = np.array(df[df['inputfile'] == 'IP3_DUMP_2']['id'])
                    file = np.array(df[df['inputfile'] == 'IP3_DUMP_2']['inputfile'])
                    diffx = [(x1 - x2) ** 2 for x1, x2 in zip(qx1, qx2)]
                    diffy = [(y1 - y2) ** 2 for y1, y2 in zip(qy1, qy2)]
                    dq = [np.log10(np.sqrt(a1 + a2)) for a1, a2 in zip(diffx, diffy)]

                    x0 = np.array(df[df['inputfile'] == 'IP3_DUMP_1']['eps1_0'])
                    y0 = np.array(df[df['inputfile'] == 'IP3_DUMP_1']['eps2_0'])
                    for i in range(len(qx2)):
                        f_res.write("%s %s %s %s %s %s %s %s %s %s\n" % (
                        id[i], file[i], qx2[i], qy2[i], dq[i], x0[i], y0[i], angle, amplitude, amplitude_1))
                        f_res.flush()

    flag_plot_fma = True

    if flag_plot_fma:
        result_folder = "SPS_VCC0.0_b3b5b7_random%s_%s_Z0.0_DPP%s_QXP2.0_QYP1.0_Q26_EN26000_fma" % (
        random, status_type, j)
        print result_folder

        df = pd.read_csv(result_folder, delim_whitespace=True, header=None)
        df.columns = ['id', 'file', 'qx', 'qy', 'dq', 'x0', 'y0', 'angle', 'amplitude', 'amplitude_1']

        pointer = 0

        # fig, axes = plt.subplots( figsize=(14.5,12.5))
        #  if pointer == 0:
        #    plt.plot(group['qx'],group['qy'], c = colors[color_pointer],label='delta ='+'{:.3f}'.format(j), linewidth=0.3)
        #    pointer = 1
        #  plt.plot(group['qx'],group['qy'], c = colors[color_pointer],label='', linewidth=0.3)
        # for key, group in df.groupby(['amplitude', 'id']):
        #  plt.plot(group['qx'],group['qy'], c = colors[color_pointer],label='', linewidth=0.3)

        # plt.plot(df['qx'],df['qy'],c = colors[color_pointer],label='delta ='+'{:.3f}'.format(j))#, linewidth=0.3)
        # plt.scatter(df['qx'], df['qy'], s=2, edgecolors=None,c = colors[color_pointer], label='delta ='+'{:.3f}'.format(j))

        # plt.scatter(df['qx'], df['qy'], s=2, edgecolors=None,c = df['amplitude_1'], label='delta ='+'{:.3f}'.format(j),cmap='jet')   #for amplitdue color map

        # try to keep specific amplitudes
        amplitudes = ['6_7', '7_8', '8_9', '9_10', '10_11', '11_12', '12_13', '13_14', '14_15']
        for amplitude in amplitudes:
            df = df[df.amplitude != amplitude]

        c1 = next(colors)
        vmin = -3.0
        vmax = -7.0
        # plt.scatter(df['qx'], df['qy'], s=2, edgecolors=None,c = df['dq'], label='delta ='+str(j),vmin=vmin,vmax=vmax,cmap='jet')  #for diffuson map
        plt.scatter(df['qx'], df['qy'], s=2, c=c1, edgecolors=None, label='delta =' + str(j))
        # bar=plt.colorbar()
        # bar.set_label(r'$\max_{j=1,2}(\log10{(\max_{i=1}^{\rm nfma}|Q_{j,i}-\bar Q_{j}|)}$',labelpad=40,rotation=270,fontsize=12)

        plt.legend()
        ''' 
        n=3
    
        plot_res_upto_order(6, l=0, qz=0.0056,c1='darkgrey', c2='darkgrey', c3='r',annotate=False)
        for ll in range (1,6):
          plot_res_order_specific([n], l=ll, qz=0.0056,c1='darkgrey', c2='darkgrey', c3='r',annotate=False)
        n=7
        for ll in range (1,7):
          plot_res_order_specific([n],list=[(4,3)], l=ll, qz=0.0056,c1='darkgrey', c2='darkgrey', c3='g',annotate=False)
        plt.tight_layout()
        '''
        # plt.xlim([0.02, 0.18]) for the dpp scan plots
        # plt.ylim([0.13, 0.27])
        plt.xlim([0.11, 0.16])
        plt.ylim([0.15, 0.20])

        plt.title('SPS, 26GeV, CC 0MV, b3b5b7,random%s_5D,\n QXP=2.0, QYP =1.0,1-6sigma, Z=200mm' % (random))
        plt.xlabel(r"$ \rm Q_x$", fontsize=14)
        plt.ylabel(r"$\rm Q_y$", fontsize=14)
        plt.tick_params(labelsize=13)
        # plt.tight_layout()
        plt.savefig("./figures/fmas_noCC_b3b5b7_random0.25_delta_resonance_3_Z200.png")
# plt.show()
plt.close()
# plt.show()
quit()



