"""
Heat map : the data values are represented in colors.
In this example we create a color map
"""
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
sys.path.append('/afs/cern.ch/user/s/skostogl/public/SPS_sixtrack/sixdb/SixDeskDB/')
import sixdeskdb

VCC = '0.0'
Qxp = '0.0'
Qyp = '0.0'
optics = '26'
dpp = '0.0'
multipoles = 'b3b5'
status = '6D'
energy = '26000'
da_fma = 'da'



# my x and y coordinates
x = np.arange(0.0, 700.0, 100.0)
y = np.arange(0.05, 1.0, 0.1)
dynamic_aperture = np.zeros(shape=(len(x),len(y)))

# The next step is to read the data from their folders
path_to_studies = '/afs/cern.ch/work/n/natriant/private/workspaces/random_multipole_errors/scratch0//w1/sixjobs'

y_pointer = 0
for yy in y: # this is the iteration for the percentages
    yy = '{0:.3g}'.format(yy)
    studies = []
    z_values = []
    min_da = []

    for xx in x:  # this is the iteration for the z values
        x_current_value = str(xx)
        new_name = 'SPS_VCC{}_{}_random{}_{}_Z{}_DPP{}_QXP{}_QYP{}_Q{}_EN{}_{}'.format(VCC, multipoles, str(yy), status,
                                                                                   x_current_value, dpp, Qxp,
                                                                                   Qyp, optics, energy, da_fma)
        studies.append(new_name)
        z_values.append(x_current_value)

    x_pointer = 0
    for study in studies:
        if os.path.exists(study + '.db'):
            # file exists
            db = sixdeskdb.SixDeskDB(study + '.db')
        else:
            db = sixdeskdb.SixDeskDB.from_dir(path_to_studies + '/studies/' + study + '/')

        db.mk_da()
        seed, angle, da = db.get_da_angle(alost='alost2').T
        dasig = da
        dasig = min(dasig.T[0])
        min_da.append(dasig)

        dynamic_aperture[x_pointer][y_pointer] = dasig
        x_pointer = x_pointer + 1
    y_pointer = y_pointer +1

print dynamic_aperture

# Now plot
fig, ax = plt.subplots()
im = ax.imshow(dynamic_aperture)

# We want to show all ticks...
ax.set_xticks(np.arange(len(x)))
ax.set_yticks(np.arange(len(y)))
# ... and label them with the respective list entries
ax.set_xticklabels(x)
ax.set_yticklabels(y)

# Rotate the tick labels and set their alignment.
plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
         rotation_mode="anchor")

# Loop over data dimensions and create text annotations.
#for i in range(len(y)):
#    for j in range(len(x)):
#        text = ax.text(j, i, z[i, j],
#                      ha="center", va="center", color="w")

ax.set_title("DA in sigma")
fig.tight_layout()
plt.show()
